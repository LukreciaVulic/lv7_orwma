package com.example.fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity implements ButtonClickListener {

    private Adapter mAdapter;
    private ViewPager mViewPager;

    private InputFragment mInputFragment;
    private MessageFragment mMessageFragment;
    private ModularFragment mModFragment1;
    private ModularFragment mModFragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    public void initialize(){
        mAdapter= new Adapter(getSupportFragmentManager());
        mViewPager=findViewById(R.id.viewPager);
        setUpPager(mViewPager);
        TabLayout tabLayout= findViewById(R.id.tab);
        tabLayout.setupWithViewPager(mViewPager);

    }

    private void setUpPager(ViewPager mViewPager) {
        mInputFragment=new InputFragment();
        mMessageFragment= new MessageFragment();
        mModFragment1= new ModularFragment(R.layout.fragment_modular);
        mModFragment2= new ModularFragment(R.layout.fragment_modular_image);
        Adapter adapter= new Adapter(getSupportFragmentManager());
        adapter.addFragment(mInputFragment,"TAB1");
        adapter.addFragment(mMessageFragment,"TAB2");
        adapter.addFragment(mModFragment1,"TAB3");
        adapter.addFragment(mModFragment2,"TAB4");
        this.mViewPager.setAdapter(adapter);

    }

    @Override
    public void onButtonClicked(String message) {
        mMessageFragment.displayMessage(message);
        this.mViewPager.setCurrentItem(1);
    }
}