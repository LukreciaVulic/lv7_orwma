package com.example.fragments;

public interface ButtonClickListener {
    void onButtonClicked(String message);
}

