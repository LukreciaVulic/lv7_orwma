package com.example.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ModularFragment extends Fragment {

    private int mLayout;

    public ModularFragment(int mLayout) {
        this.mLayout = mLayout;
    }

    public static ModularFragment newInstance(int layout) {
        return new ModularFragment(layout);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(mLayout, container, false);
    }




}