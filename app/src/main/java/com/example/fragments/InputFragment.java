package com.example.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;


public class InputFragment extends Fragment implements TextWatcher {

    private String mMessage="Hello";
    private EditText mEditText;
    private Button mSubmitButton;
    private ButtonClickListener mButtonClickListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditText=view.findViewById(R.id.etMessage);
        mSubmitButton= view.findViewById(R.id.btnSubmit);
        setUpListeners();
    }

    private void setUpListeners() {
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButtonClickListener.onButtonClicked(mMessage);
                mEditText.onEditorAction(EditorInfo.IME_ACTION_DONE);
            }
        });
        mEditText.addTextChangedListener(this);
        mEditText.clearComposingText();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof ButtonClickListener){
            this.mButtonClickListener=(ButtonClickListener)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mButtonClickListener=null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mMessage=s.toString();
    }

    public static InputFragment newInstance() {
        return new InputFragment();
    }
}